CREATE DATABASE `campanhas` /*!40100 DEFAULT CHARACTER SET utf8 */;

CREATE TABLE `teams` (
  `team_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`team_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;


CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(200) NOT NULL,
  `email` varchar(100) NOT NULL,
  `birthdate` date DEFAULT NULL,
  `team_id` int(11) DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `last_updated_at` datetime DEFAULT NULL,
  `partner_campaign` int(1) DEFAULT '0',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `users_campaigns` (
  `user_id` int(11) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `last_updated_at` datetime DEFAULT NULL,
  `team_id` int(11) DEFAULT NULL,
  `full_name` varchar(100) DEFAULT NULL,
  `finish_date` datetime DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`,`campaign_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `teams` (`team_id`,`name`) VALUES (1,'Corinthians');
INSERT INTO `teams` (`team_id`,`name`) VALUES (2,'Palmeiras');
INSERT INTO `teams` (`team_id`,`name`) VALUES (3,'Santos');
INSERT INTO `teams` (`team_id`,`name`) VALUES (4,'Gr�mio');
INSERT INTO `teams` (`team_id`,`name`) VALUES (5,'Cruzeiro');
INSERT INTO `teams` (`team_id`,`name`) VALUES (6,'Botafogo');
INSERT INTO `teams` (`team_id`,`name`) VALUES (7,'Flamengo');
INSERT INTO `teams` (`team_id`,`name`) VALUES (8,'Vasco');
INSERT INTO `teams` (`team_id`,`name`) VALUES (9,'Atl�tico-PR');
INSERT INTO `teams` (`team_id`,`name`) VALUES (10,'Atl�tico-MG');
INSERT INTO `teams` (`team_id`,`name`) VALUES (11,'S�o Paulo');
INSERT INTO `teams` (`team_id`,`name`) VALUES (12,'Bahia');
INSERT INTO `teams` (`team_id`,`name`) VALUES (13,'Fluminense');
INSERT INTO `teams` (`team_id`,`name`) VALUES (14,'Sport');
INSERT INTO `teams` (`team_id`,`name`) VALUES (15,'Ava�');
INSERT INTO `teams` (`team_id`,`name`) VALUES (16,'Chapecoense');
INSERT INTO `teams` (`team_id`,`name`) VALUES (17,'Vit�ria');
INSERT INTO `teams` (`team_id`,`name`) VALUES (18,'Ponte Preta');
INSERT INTO `teams` (`team_id`,`name`) VALUES (19,'Coritiba');
INSERT INTO `teams` (`team_id`,`name`) VALUES (20,'Atl�tico-GO');

INSERT INTO `users` (`user_id`,`full_name`,`email`,`birthdate`,`team_id`,`created_by`,`created_at`,`updated_by`,`last_updated_at`,`partner_campaign`) VALUES (1,'Felipe Simplicio Moraes da Silva','felipemonteverde@gmail.com','1982-08-17',2,'felipe','2018-03-25 16:44:47','felipe','2018-03-25 17:29:30',0);
INSERT INTO `users` (`user_id`,`full_name`,`email`,`birthdate`,`team_id`,`created_by`,`created_at`,`updated_by`,`last_updated_at`,`partner_campaign`) VALUES (2,'Felipe Silva','felipesilva@gmail.com','1982-08-17',1,'felipe','2018-03-25 17:22:07','felipe','2018-03-25 17:30:52',0);

INSERT INTO `users_campaigns` (`user_id`,`campaign_id`,`created_by`,`created_at`,`updated_by`,`last_updated_at`,`team_id`,`full_name`,`finish_date`,`start_date`) VALUES (1,3,'felipe','2018-03-25 16:44:49','felipe','2018-03-25 17:29:31',2,'Campanha 3','2018-10-03 00:00:00','2018-01-01 00:00:00');
INSERT INTO `users_campaigns` (`user_id`,`campaign_id`,`created_by`,`created_at`,`updated_by`,`last_updated_at`,`team_id`,`full_name`,`finish_date`,`start_date`) VALUES (2,1,NULL,'2018-03-25 17:30:52',NULL,NULL,1,'Campanha 1','2018-10-05 00:00:00','2018-01-01 00:00:00');
INSERT INTO `users_campaigns` (`user_id`,`campaign_id`,`created_by`,`created_at`,`updated_by`,`last_updated_at`,`team_id`,`full_name`,`finish_date`,`start_date`) VALUES (2,2,NULL,'2018-03-25 17:30:53',NULL,NULL,1,'Campanha 2','2018-10-04 00:00:00','2018-01-01 00:00:00');




