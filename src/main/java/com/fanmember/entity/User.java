package com.fanmember.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="users")
public class User implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="user_id")
    private int userId;  
	@Column(name="full_name")
    private String fullName;	
	@Column(name="birthdate")
    private Date birthdate;
	@Column(name="email")
    private String email;	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="team_id")
	private Team team;
	
	@OneToMany(fetch=FetchType.EAGER)
	@JoinColumn(name = "user_id")
	private List<UserCampaign> campaigns;
		
	@Column(name="created_by")
	private String createdBy;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_at", updatable = false, nullable = false)
	private Date createdAt;
	
	@Column(name="updated_by")
	private String updatedBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="last_updated_at", insertable = false, nullable = false)
	private Date lastUpdateAt;
	
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public Date getBirthdate() {
		return birthdate;
	}
	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}		
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Team getTeam() {
		return team;
	}
	public void setTeam(Team team) {
		this.team = team;
	}
	public List<UserCampaign> getCampaigns() {
		return campaigns;
	}
	public void setCampaigns(List<UserCampaign> campaigns) {
		this.campaigns = campaigns;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getLastUpdateAt() {
		return lastUpdateAt;
	}
	public void setLastUpdateAt(Date lastUpdateAt) {
		this.lastUpdateAt = lastUpdateAt;
	}		

	@PrePersist
	public void createdAt() {
		this.createdAt = new Date();
	}
	@PreUpdate
	public void updatedAt() {
		this.lastUpdateAt = new Date();
	}
	
}
