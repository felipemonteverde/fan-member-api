package com.fanmember.dao;

import java.util.List;

import com.fanmember.entity.User;

public interface IUserDao {
	public List<User> getAllUsers(String email);
	public User getUserById(int userId);
    public void createUser(User user);
    public void mergeUser(User user);
    public void updateUser(User user);
    public void deleteUser(int userId);

}
