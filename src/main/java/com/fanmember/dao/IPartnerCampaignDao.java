package com.fanmember.dao;

import java.util.List;

import com.fanmember.entity.UserCampaign;

public interface IPartnerCampaignDao {

	public void createPartnerCampaign(UserCampaign userCampaign);
	public void mergePartnerCampaign(UserCampaign userCampaign);
	public List<UserCampaign> getPartnerCampaigns(Integer userId);
	public UserCampaign getPartnerCampaign(Integer userId, Integer campaignId);
}
