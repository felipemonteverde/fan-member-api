package com.fanmember.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import com.fanmember.entity.Team;
import com.fanmember.entity.User;

@Transactional
public class UserDao implements IUserDao {

	@PersistenceContext	
	private EntityManager entityManager;
	
	List<User> list = new ArrayList<User>();
	List<Team> teams = new ArrayList<Team>();	
	
	@Override
	public List<User> getAllUsers(String email) {
		if(email != null) {
			String hql = "FROM User as u WHERE u.email = ? ORDER BY u.userId";
			return (List<User>) entityManager.createQuery(hql).setParameter(1, email).getResultList();
		} else {
			String hql = "FROM User as u ORDER BY u.userId";
			return (List<User>) entityManager.createQuery(hql).getResultList();
		}
	}

	@Override
	public User getUserById(int userId) {
		return entityManager.find(User.class, userId);
	}

	@Override
	public void createUser(User user) {			
		entityManager.persist(user);  	    
	}
	@Override
	public void mergeUser(User user) {			
		entityManager.merge(user);  	    
	}
	
	@Override
	public void updateUser(User user) {
		User userl = getUserById(user.getUserId());
		userl.setFullName(user.getFullName());
		userl.setEmail(user.getEmail());
		userl.setBirthdate(user.getBirthdate());
		userl.setUpdatedBy(user.getUpdatedBy());
		userl.setTeam(user.getTeam());
		entityManager.flush();	
	}

	@Override
	public void deleteUser(int userId) {
		entityManager.remove(getUserById(userId));	
	}	
}
