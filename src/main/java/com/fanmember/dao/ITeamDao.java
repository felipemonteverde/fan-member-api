package com.fanmember.dao;

import java.util.List;

import com.fanmember.entity.Team;


public interface ITeamDao {
	List<Team> getAllTeams();
	Team getTeamById(int teamId);
}
