package com.fanmember.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fanmember.entity.User;
import com.fanmember.entity.UserCampaign;

import io.swagger.annotations.ApiModelProperty;

public class UserResponseDTO {

	@ApiModelProperty(notes = "The database generated userId")
	private int userId;  
	@ApiModelProperty(notes = "The full name of the user")
	private String fullName;	
	@ApiModelProperty(notes = "The birthdate of the user")
	private Date birthdate;
	@ApiModelProperty(notes = "The name of the user in format yyyy-MM-dd'T'HH:mm:ss.SSSZ")
	private String email;	
	@ApiModelProperty(notes = "The name of the user")
	private TeamDTO team;
	@ApiModelProperty(notes = "The campaign of the user partner")
	private List<CampaignDTO> campaigns;
	@ApiModelProperty(notes = "User that create the user")
	private String createdBy;		
	@ApiModelProperty(notes = "Date that user's create")
	private Date createdAt;	
	@ApiModelProperty(notes = "User that updated the user")
	private String updatedBy;	
	@ApiModelProperty(notes = "Date that user's updated")
	private Date lastUpdateAt;
	
	public UserResponseDTO() {}
	public UserResponseDTO(User user) {		
		this.userId = user.getUserId();  
		this.fullName = user.getFullName();	
		this.birthdate = user.getBirthdate();
		this.email = user.getEmail();
		this.createdBy = user.getCreatedBy();
		this.createdAt = user.getCreatedAt();	
		this.updatedBy = user.getUpdatedBy();	
		this.lastUpdateAt = user.getLastUpdateAt();
		if(user.getTeam() != null) {			
			this.team = new TeamDTO(user.getTeam());
		}
		if(user.getCampaigns() != null) {
			List<CampaignDTO> campaignsDTO = new ArrayList<CampaignDTO>();
			for(UserCampaign userCampaign :user.getCampaigns()) {
				campaignsDTO.add(new CampaignDTO(userCampaign));
			}
			this.campaigns = campaignsDTO;
		}
	}
	
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public Date getBirthdate() {
		return birthdate;
	}
	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public List<CampaignDTO> getCampaigns() {
		return campaigns;
	}
	public void setCampaigns(List<CampaignDTO> campaigns) {
		this.campaigns = campaigns;
	}
	public TeamDTO getTeam() {
		return team;
	}
	public void setTeam(TeamDTO team) {
		this.team = team;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getLastUpdateAt() {
		return lastUpdateAt;
	}
	public void setLastUpdateAt(Date lastUpdateAt) {
		this.lastUpdateAt = lastUpdateAt;
	}
	
}
