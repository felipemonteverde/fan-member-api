package com.fanmember.dto;

import java.util.Date;

import com.fanmember.entity.UserCampaign;

import io.swagger.annotations.ApiModelProperty;

public class CampaignDTO {

	@ApiModelProperty(notes = "The database generated campaignId")
	private int campaignId;  
	@ApiModelProperty(notes = "The name of the campaign")
	private String fullName;
	@ApiModelProperty(notes = "The start date of the campaign in format yyyy-MM-dd'T'HH:mm:ss.SSSZ")
	private Date startDate;
	@ApiModelProperty(notes = "The finish date of the campaign in format yyyy-MM-dd'T'HH:mm:ss.SSSZ")
	private Date finishDate;
	@ApiModelProperty(notes = "User that create the campaign")
	private String createdBy;
	@ApiModelProperty(notes = "Date that campaign's create")
	private Date createdAt;
	@ApiModelProperty(notes = "User that updated the campaign")
	private String updatedBy;
	@ApiModelProperty(notes = "Date that campaign's updated")
	private Date lastUpdateAt;
	
	public CampaignDTO() {}
	public CampaignDTO(UserCampaign userCampaign) {
		this.campaignId = userCampaign.getCampaignId();  
		this.fullName = userCampaign.getFullName();
		this.startDate = userCampaign.getStartDate();
		this.finishDate = userCampaign.getFinishDate();
		this.createdBy = userCampaign.getCreatedBy();
		this.createdAt = userCampaign.getCreatedAt();
		this.updatedBy = userCampaign.getUpdatedBy();
		this.lastUpdateAt = userCampaign.getLastUpdateAt();
	}
	
	public int getCampaignId() {
		return campaignId;
	}
	public void setCampaignId(int campaignId) {
		this.campaignId = campaignId;
	}	
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getFinishDate() {
		return finishDate;
	}
	public void setFinishDate(Date finishDate) {
		this.finishDate = finishDate;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getLastUpdateAt() {
		return lastUpdateAt;
	}
	public void setLastUpdateAt(Date lastUpdateAt) {
		this.lastUpdateAt = lastUpdateAt;
	}
	
}
