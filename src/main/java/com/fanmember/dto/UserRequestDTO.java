package com.fanmember.dto;

import java.util.Date;

import javax.validation.constraints.NotNull;

import com.fanmember.entity.Team;
import com.fanmember.entity.User;

import io.swagger.annotations.ApiModelProperty;

public class UserRequestDTO {

	@ApiModelProperty(notes = "The full name of the user")
	@NotNull(message = "Field \"fullName\" is required!!")
	private String fullName;
	@ApiModelProperty(notes = "The birthdat of the user in format yyyy-MM-dd'T'HH:mm:ss.SSSZ")
	@NotNull(message = "Field \"birthdate\" is required!!")
	private Date birthdate;
	@ApiModelProperty(notes = "The email of the user")
	@NotNull(message = "Field \"email\" is required!!")
	private String email;	
	@ApiModelProperty(notes = "The team of the user")
	@NotNull(message = "Field \"team\" is required!!")
	private TeamDTO team;
	
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public Date getBirthdate() {
		return birthdate;
	}
	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public TeamDTO getTeam() {
		return team;
	}
	public void setTeam(TeamDTO team) {
		this.team = team;
	}
	
	public User toUser(){
		User user = new User();
		user.setFullName(this.fullName);
		user.setEmail(this.email);
		user.setBirthdate(this.birthdate);
		if(this.team != null) {
			Team team = new Team();
			team.setTeamId(this.team.getTeamId());
			user.setTeam(team);
		}
		return user;
	}
	
}
