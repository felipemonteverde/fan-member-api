package com.fanmember.domain;

import java.io.Serializable;

import org.joda.time.DateTime;

import io.swagger.annotations.ApiModelProperty;

public class ErrorMessage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModelProperty(notes = "The code of error")
	public String code;
	@ApiModelProperty(notes = "The message of error")
	public String message;
	@ApiModelProperty(notes = "The date that error occorer")
	public DateTime date;
	
	public ErrorMessage() {}
	
	public ErrorMessage(String code, String message, DateTime date) {
		this.code = code;
		this.message = message;
		this.date = date;
	}
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public DateTime getDate() {
		return date;
	}
	public void setDate(DateTime date) {
		this.date = date;
	}
	
}
