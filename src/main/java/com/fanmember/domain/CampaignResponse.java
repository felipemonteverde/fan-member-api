package com.fanmember.domain;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModelProperty;

public class CampaignResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(notes = "The database generated campaignId")
	private int campaignId;  
	@ApiModelProperty(notes = "The name of the campaign")
	private String name;	
	@ApiModelProperty(notes = "The start date of the campaign in format yyyy-MM-dd'T'HH:mm:ss.SSSZ")
	private Date startDate;
	@ApiModelProperty(notes = "The finish date of the campaign in format yyyy-MM-dd'T'HH:mm:ss.SSSZ")
	private Date finishDate;
	@ApiModelProperty(notes = "The team of the campaign")	
	private Team team;
	@ApiModelProperty(notes = "User that create the campaign")
	private String createdBy;	
	@ApiModelProperty(notes = "Date that campaign's create")
	private Date createdAt;	
	@ApiModelProperty(notes = "User that updated the campaign")
	private String updatedBy;
	@ApiModelProperty(notes = "Date that campaign's updated")
	private Date lastUpdateAt;
	
	public int getCampaignId() {
		return campaignId;
	}
	public void setCampaignId(int campaignId) {
		this.campaignId = campaignId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getFinishDate() {
		return finishDate;
	}
	public void setFinishDate(Date finishDate) {
		this.finishDate = finishDate;
	}
	public Team getTeam() {
		return team;
	}
	public void setTeam(Team team) {
		this.team = team;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getLastUpdateAt() {
		return lastUpdateAt;
	}
	public void setLastUpdateAt(Date lastUpdateAt) {
		this.lastUpdateAt = lastUpdateAt;
	}
			
}

