package com.fanmember.domain;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;

public class Team implements Serializable {

	private static final long serialVersionUID = 1L;
	@ApiModelProperty(notes = "The database generated teamId")
	private int teamId;  
	@ApiModelProperty(notes = "The name of the team")
	private String name;	

	public int getTeamId() {
		return teamId;
	}
	public void setTeamId(int teamId) {
		this.teamId = teamId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
