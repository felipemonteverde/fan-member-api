package com.fanmember.controller;

import java.util.List;

import javax.validation.Valid;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.fanmember.domain.CampaignResponse;
import com.fanmember.domain.ErrorMessage;
import com.fanmember.dto.UserRequestDTO;
import com.fanmember.dto.UserResponseDTO;
import com.fanmember.entity.User;
import com.fanmember.exception.BusinessException;
import com.fanmember.service.IUserService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@RestController
@RequestMapping("user")
public class UserController {

	@Autowired
	private IUserService userService;	
	
	@SuppressWarnings("rawtypes")
	@ApiOperation(value = "Get User", nickname = "Get User" )
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Success", response = CampaignResponse.class, responseContainer="List"),            
            @ApiResponse(code = 404, message = "Not Found", response = ErrorMessage.class),
            @ApiResponse(code = 500, message = "Failure", response = ErrorMessage.class)})
	@RequestMapping(method = RequestMethod.GET, path="", produces = "application/json")
	public @ResponseBody ResponseEntity getUser(@RequestParam(value="email") String email) {
		try {
			List<CampaignResponse> list = userService.getUserByEmail(email);
			return new ResponseEntity<List<CampaignResponse>>(list, HttpStatus.OK);
		} catch (BusinessException e) {			
			return new ResponseEntity<ErrorMessage>(new ErrorMessage("0", e.getMessage(), new DateTime()), HttpStatus.NOT_FOUND);
		} catch (Exception e) {			
			return new ResponseEntity<ErrorMessage>(new ErrorMessage("0", e.getMessage(), new DateTime()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	
	@SuppressWarnings("rawtypes")
	@ApiOperation(value = "Get Users", nickname = "Get Users" )
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Success", response = UserResponseDTO.class, responseContainer="List"),            
            @ApiResponse(code = 404, message = "Not Found", response = ErrorMessage.class),
            @ApiResponse(code = 500, message = "Failure", response = ErrorMessage.class)}) 
	@RequestMapping(method = RequestMethod.GET, path="/all", produces = "application/json")
	public @ResponseBody ResponseEntity getAllUsers() {		
		try {
			List<UserResponseDTO> list = userService.getAllUsers(null);
			return new ResponseEntity<List<UserResponseDTO>>(list, HttpStatus.OK);
		} catch (Exception e) {			
			return new ResponseEntity<ErrorMessage>(new ErrorMessage("0", e.getMessage(), new DateTime()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
			
	@SuppressWarnings("rawtypes")
	@ApiOperation(value = "Get User By Id", nickname = "Get User By Id" )
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Success", response = UserResponseDTO.class ),            
            @ApiResponse(code = 404, message = "Not Found", response = ErrorMessage.class),
            @ApiResponse(code = 500, message = "Failure", response = ErrorMessage.class)})
	@RequestMapping(method = RequestMethod.GET, path="/{id}", produces = "application/json")	
	public @ResponseBody ResponseEntity getCampaignById(@PathVariable("id") int id) {
		try {
			UserResponseDTO user = userService.getUserById(id);
			return new ResponseEntity<UserResponseDTO>(user, HttpStatus.OK);
		} catch (BusinessException e) {			
			return new ResponseEntity<ErrorMessage>(new ErrorMessage("0", e.getMessage(), new DateTime()), HttpStatus.NOT_FOUND);
		} catch (Exception e) {			
			return new ResponseEntity<ErrorMessage>(new ErrorMessage("0", e.getMessage(), new DateTime()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings("rawtypes")
	@ApiOperation(value = "Create User", nickname = "Create User" )
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Success", response = UserResponseDTO.class),
            @ApiResponse(code = 201, message = "Criated", response = Void.class),
            @ApiResponse(code = 400, message = "Bad Request", response = UserResponseDTO.class),
            @ApiResponse(code = 409, message = "Conflict", response = ErrorMessage.class),
            @ApiResponse(code = 500, message = "Failure", response = ErrorMessage.class)}) 
	@RequestMapping(method = RequestMethod.POST, path="/new", produces = "application/json")
	public @ResponseBody ResponseEntity createUser(@RequestHeader("userId") String userId, @RequestBody @Valid UserRequestDTO userReq, UriComponentsBuilder builder) {
       try {
    	   User user = userService.createUser(userReq, userId);    	  
    	   HttpHeaders headers = new HttpHeaders();
    	   headers.setLocation(builder.path("/user/{id}").buildAndExpand(user.getUserId()).toUri());
    	   return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
       } catch (BusinessException e) {			
			return new ResponseEntity<ErrorMessage>(new ErrorMessage("0", e.getMessage(), new DateTime()), HttpStatus.CONFLICT);
       } catch (Exception e) {			
			return new ResponseEntity<ErrorMessage>(new ErrorMessage("0", e.getMessage(), new DateTime()), HttpStatus.INTERNAL_SERVER_ERROR);
       }
	}	
	
	@SuppressWarnings("rawtypes")
	@ApiOperation(value = "Remove User", nickname = "Remove User" )
	@ApiResponses(value = {			
            @ApiResponse(code = 204, message = "No Content", response = ErrorMessage.class),
            @ApiResponse(code = 404, message = "Conflict", response = ErrorMessage.class),
            @ApiResponse(code = 500, message = "Failure", response = ErrorMessage.class)})
	@RequestMapping(method = RequestMethod.DELETE, path="/{id}", produces = "application/json")
	public ResponseEntity deleteCampaign(@PathVariable("id") int id) {
		try {
			userService.deleteUser(id);
			return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
		} catch (BusinessException e) {			
			return new ResponseEntity<ErrorMessage>(new ErrorMessage("0", e.getMessage(), new DateTime()), HttpStatus.NOT_FOUND);
		} catch (Exception e) {			
			return new ResponseEntity<ErrorMessage>(new ErrorMessage("0", e.getMessage(), new DateTime()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}	
	
	@SuppressWarnings("rawtypes")
	@ApiOperation(value = "Update User", nickname = "Update User" )
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Success", response = UserResponseDTO.class),
            @ApiResponse(code = 204, message = "No Content", response = Void.class),
            @ApiResponse(code = 400, message = "Bad Request", response = UserResponseDTO.class),            
            @ApiResponse(code = 404, message = "Not Found", response = ErrorMessage.class),
            @ApiResponse(code = 500, message = "Failure", response = ErrorMessage.class)}) 
	@RequestMapping(method = RequestMethod.PUT, path="/{id}", produces = "application/json")
	public @ResponseBody ResponseEntity updateUser(@PathVariable("id") int id, @RequestHeader("userId") String userId, @RequestBody @Valid UserRequestDTO userReq) {
		try {			
			userService.updateUser(id, userReq, userId);
			return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
		} catch (BusinessException e) {			
			return new ResponseEntity<ErrorMessage>(new ErrorMessage("0", e.getMessage(), new DateTime()), HttpStatus.NOT_FOUND);
		} catch (Exception e) {			
			return new ResponseEntity<ErrorMessage>(new ErrorMessage("0", e.getMessage(), new DateTime()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}	
}
