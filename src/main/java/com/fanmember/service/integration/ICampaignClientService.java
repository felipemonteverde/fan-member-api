package com.fanmember.service.integration;

import java.util.List;

import com.fanmember.domain.CampaignResponse;
import com.fanmember.entity.User;

public interface ICampaignClientService {
	
	public List<CampaignResponse> getCampaigns(Integer teamId);
	
	public boolean setUserPartnerCampaign(User user);

}
