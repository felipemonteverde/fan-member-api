package com.fanmember.service.integration;

import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.springframework.stereotype.Service;

import com.fanmember.domain.CampaignResponse;
import com.fanmember.entity.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.joda.JodaModule;

@Service
public class CampaignClientService implements ICampaignClientService {

	@Override
	public List<CampaignResponse> getCampaigns(Integer teamId) {
		
		List<CampaignResponse> campaigns = null;
		try {
			ClientBuilder.newBuilder();						
			Client client = ClientBuilder.newClient();
			
			WebTarget target = client.target("http://localhost:8080/campaign-api/campaign");			
			target = target.path("/all");
			 
			Invocation invocation = target.queryParam("teamId", teamId).request().buildGet();
			String json = invocation.invoke(String.class);
			
			ObjectMapper mapper = new ObjectMapper();
			mapper.registerModule(new JodaModule());
		
			campaigns = mapper.readValue(json, mapper.getTypeFactory().constructCollectionType(List.class, CampaignResponse.class));
			
		} catch (Exception e) {
			return null;
		}				
			
		return campaigns;
	}

	@Override
	public boolean setUserPartnerCampaign(User user) {
		
		try {
			ClientBuilder.newBuilder();						
			Client client = ClientBuilder.newClient();
			
			WebTarget target = client.target("http://localhost:8080/campaign-api/partner-campaign");			
			target = target.path("/new");			
			target.request(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON_TYPE)
            .header("userId", "fan-member")            
            .post(Entity.json("{\"userId\":"+user.getUserId()+", \"teamId\": "+user.getTeam().getTeamId()+"}"), String.class);
			
			return true;
		} catch (Exception e) {
			return false;
		}
		
	}
	
	
}
