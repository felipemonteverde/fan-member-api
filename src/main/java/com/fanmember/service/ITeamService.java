package com.fanmember.service;

import java.util.List;

import com.fanmember.dto.TeamDTO;
import com.fanmember.exception.BusinessException;

public interface ITeamService {

	List<TeamDTO> getAllTeams();
	TeamDTO getTeamById(int teamId) throws BusinessException;
}
