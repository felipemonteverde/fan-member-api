package com.fanmember.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fanmember.dao.IPartnerCampaignDao;
import com.fanmember.dao.ITeamDao;
import com.fanmember.dao.IUserDao;
import com.fanmember.domain.CampaignResponse;
import com.fanmember.dto.UserRequestDTO;
import com.fanmember.dto.UserResponseDTO;
import com.fanmember.entity.Team;
import com.fanmember.entity.User;
import com.fanmember.entity.UserCampaign;
import com.fanmember.exception.BusinessException;
import com.fanmember.service.integration.ICampaignClientService;

@Service
public class UserService implements IUserService {
	
	@Autowired
	private IUserDao userDao;
	
	@Autowired
	private ITeamDao teamDao;
	
	@Autowired
	private IPartnerCampaignDao partnerCampaignDao;
	
	@Autowired
	private ICampaignClientService campaignClientService;
			
	@Override
	public List<UserResponseDTO> getAllUsers(String email) {
		List<User> users = getUsers(email);
		List<UserResponseDTO> list = new ArrayList<UserResponseDTO>();
		for (User user : users) {
			list.add(new UserResponseDTO(user));
		}
		return list;
	}
	
	private List<User> getUsers(String email) {
		return userDao.getAllUsers(email);
	}

	@Override
	public UserResponseDTO getUserById(int userId) throws BusinessException {		
		User user  = userDao.getUserById(userId);
		if(user == null) {
			throw new BusinessException("Cadastro nao encontrado "+ userId);
		}		
		return new UserResponseDTO(user);
	}

	@Override
	public User createUser(UserRequestDTO userReq, String userId) throws BusinessException {
		User user = userReq.toUser();
		
		List<User> list = getUsers(user.getEmail());
		if(list != null && !list.isEmpty()) {
			throw new BusinessException("Email ja existente para outro usuario");
		}
		if(user.getTeam() != null && user.getTeam().getTeamId() > 0) {
			Team team = teamDao.getTeamById(user.getTeam().getTeamId());
			if(team == null) {
				throw new BusinessException("Time invalido");
			}
			user.setTeam(team);
		}
		
		user.setCreatedBy(userId);		
		userDao.createUser(user);		
		addUserPartnerCampaign(user);
		return user;
	}
		
	@Override
	public void updateUser(int id, UserRequestDTO userReq, String userId) throws BusinessException {
		User user = userReq.toUser();
		user.setUserId(id);
		user.setUpdatedBy(userId);
		userDao.updateUser(user);		
		addUserPartnerCampaign(user);
	}

	@Override
	public void deleteUser(int userId) throws BusinessException {		
		userDao.deleteUser(userId);		
	}

	@Override
	public List<CampaignResponse> getUserByEmail(String email) throws BusinessException {
		List<User> users = getUsers(email);
		if(users != null && !users.isEmpty()) {
			User user = users.get(0);
			if(!user.getCampaigns().isEmpty()) {
				throw new BusinessException("Cadastro j� foi efetuado");
			}
			
			List<CampaignResponse> campaigns = campaignClientService.getCampaigns(user.getTeam().getTeamId());
			return campaigns;
						
		}
		
		return null;
	}	
	
	private void addUserPartnerCampaign(User user) {
		List<CampaignResponse> campaigns = campaignClientService.getCampaigns(user.getTeam().getTeamId());
		if(campaigns != null && !campaigns.isEmpty()) {
			for (CampaignResponse campaignResponse : campaigns) {
				UserCampaign partnerCampaign = partnerCampaignDao.getPartnerCampaign(user.getUserId(), campaignResponse.getCampaignId());
				
				UserCampaign userCampaign = new UserCampaign();
				userCampaign.setFullName(campaignResponse.getName());
				userCampaign.setStartDate(campaignResponse.getStartDate());
				userCampaign.setFinishDate(campaignResponse.getFinishDate());
				userCampaign.setCampaignId(campaignResponse.getCampaignId());
				userCampaign.setUserId(user.getUserId());
				userCampaign.setTeamId(user.getTeam().getTeamId());					
				if(partnerCampaign == null) {
					userCampaign.setCreatedBy(user.getCreatedBy());
					partnerCampaignDao.createPartnerCampaign(userCampaign);
				} else {				
					userCampaign.setUpdatedBy(user.getUpdatedBy());
					partnerCampaignDao.mergePartnerCampaign(userCampaign);
				}
			}
		}		
	}

}
