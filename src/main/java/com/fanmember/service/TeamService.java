package com.fanmember.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fanmember.dao.ITeamDao;
import com.fanmember.dto.TeamDTO;
import com.fanmember.entity.Team;
import com.fanmember.exception.BusinessException;

@Service
public class TeamService implements ITeamService{

	@Autowired
	private ITeamDao teamDao;
	
	@Override
	public List<TeamDTO> getAllTeams() {
		List<Team> teams = teamDao.getAllTeams();
		List<TeamDTO> list = new ArrayList<TeamDTO>();
		for (Team team : teams) {
			list.add(new TeamDTO(team));
		}
		return list;
	}

	@Override
	public TeamDTO getTeamById(int teamId) throws BusinessException {		
		Team team = teamDao.getTeamById(teamId);
		return new TeamDTO(team);
	}

}
