package com.fanmember.service;

import java.util.List;

import com.fanmember.domain.CampaignResponse;
import com.fanmember.dto.UserRequestDTO;
import com.fanmember.dto.UserResponseDTO;
import com.fanmember.entity.User;
import com.fanmember.exception.BusinessException;

public interface IUserService {
	
	public List<UserResponseDTO> getAllUsers(String email);
	public UserResponseDTO getUserById(int userId) throws BusinessException;
    public User createUser(UserRequestDTO userReq, String userId) throws BusinessException;
    public void updateUser(int id, UserRequestDTO user, String userId) throws BusinessException;
    public void deleteUser(int userId) throws BusinessException;
    public List<CampaignResponse> getUserByEmail(String email) throws BusinessException;

}
